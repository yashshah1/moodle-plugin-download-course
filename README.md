## Course Download Block Plugin
A simple block plugin that downloads a moodle course contents as a zip

## How to run
1. Clone this using `git clone git@gitlab.com:yashshah1/moodle-plugin-download-course.git course_download`
2. Copy this folder into the blocks folder of your moodle installation, found at `moodlecode/blocks/`
3. Log into moodle as admin


## Only supports files, folders, pages and assignments
