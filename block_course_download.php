<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Course Download Block
 *
 * @package    block_course_download
 * @copyright  2019 onwards Yash Shah(yashah1234@gmail.com) and Kryselle Martis(krysellesimaranmartis@gmail.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Course download block class.
 *
 * @copyright  2019 onwards Yash Shah(yashah1234@gmail.com) and Kryselle Martis(krysellesimaranmartis@gmail.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class block_course_download extends block_base {
    /**
     * Init.
     */
    public function init() {
        $this->title = get_string('material_download', 'block_course_download');
    }
    /**
     * Returns the contents.
     *
     * @return stdClass contents of block
     */
    public function get_content() {
        global $DB, $CFG, $OUTPUT, $COURSE, $PAGE;
        require_once("$CFG->libdir/resourcelib.php");
        if ($this->content !== null) {
            return $this->content;
        }
        $this->content = new stdClass;
        $filesflag            = true; // HOTFIX, Todo, if there are no files.
        $this->content->footer = '';
        if ($filesflag) {
            $url = new moodle_url('/blocks/course_download/view.php', array('courseid' => $COURSE->id));
            $this->content->footer .= html_writer::link($url, get_string('download_files', 'block_course_download'));
        }
        // TODO; add support if there are no files.
        return $this->content;
    }

    /**
     * Locations where block can be displayed.
     *
     * @return array
     */
    public function applicable_formats() {
        return array(
                     'my' => false,
                     'course-view' => true,
                     'course-view-social' => true
                     );
    }
}

