<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Course Download Block
 *
 * @package    block_course_download
 * @copyright  2019 onwards Yash Shah(yashah1234@gmail.com) and Kryselle Martis(krysellesimaranmartis@gmail.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

/**
 * To get all pages in a particular course
 *
 * @copyright  2019 onwards Yash Shah(yashah1234@gmail.com) and Kryselle Martis(krysellesimaranmartis@gmail.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class page {
    /**
     *
     * Class constructor
     *
     * @param int $courseid The course id of the respective course.
     */
    public function __construct($courseid) {
        GLOBAL $DB;
        $this->name = get_string('page', 'block_course_download');
        $this->courseid = $courseid;
        $this->db = $DB;
    }
    /**
     *
     * To get all pages in a course
     *
     * @return array $filestodownload Associative array with file path as key and value from the database as its value
     *
     */
    public function get_material() {

        $course   = $this->db->get_record('course', array('id' => $this->courseid), '*', MUST_EXIST);
        $filestodownload = array();
        $modulenumberofpage = $this->db->get_records('modules', array('name' => 'page'));
        $modulenumberofpage = (int) current($modulenumberofpage)->id;

        if ($course->format == "topics") {
            $subfolder = get_string('topic', 'block_course_download');
        } else {
            $subfolder = get_string('week',  'block_course_download');
        }

        $sectionrecords = $this->db->get_records('course_sections', array('course' => $this->courseid));
        foreach ($sectionrecords as $sectionrecord) {
            $sectid = $sectionrecord->section;
            $sequence = explode(',',    $sectionrecord->sequence);
            for ($i = 0; $i < count($sequence); $i++) {
                $sequenceid = $sequence[$i];
                $moduleofsequence = $this->db->get_records('course_modules', array('id' => $sequenceid), '');
                $sequencemodule = (int) current($moduleofsequence)->module;
                if ($sequencemodule === $modulenumberofpage) {
                    $instance = (int) current($moduleofsequence)->instance;
                    $instancerecord = $this->db->get_records('page', array('id' => $instance));
                    $instancerecord = current($instancerecord);

                    $pagename = clean_filename($instancerecord->name);
                    $contentofpage  = "";
                    $contentofpage .= "Intro:\n-------------\n";
                    $contentofpage .= $instancerecord->intro;
                    $contentofpage .= "\n\nContent:\n-------------\n";
                    $contentofpage .= $instancerecord->content;
                    $contentofpage  = array($contentofpage); // So that it gets written to a file.
                    $directory = $subfolder.'_'.$sectid.'/page/';
                    $filestodownload[$directory.$pagename] = $contentofpage;

                }
            }
        }
        return $filestodownload;
    }
}