<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Course Download Block
 *
 * @package    block_course_download
 * @copyright  2019 onwards Yash Shah(yashah1234@gmail.com) and Kryselle Martis(krysellesimaranmartis@gmail.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

/**
 * To get all uploaded folders in a particular course
 *
 * @copyright  2019 onwards Yash Shah(yashah1234@gmail.com) and Kryselle Martis(krysellesimaranmartis@gmail.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class folder {
    /**
     *
     * Class constructor
     *
     * @param int $courseid The course id of the respective course.
     */
    public function __construct($courseid) {
        GLOBAL $DB;
        $this->name = get_string('folder', 'block_course_download');
        $this->courseid = $courseid;
        $this->db = $DB;
    }
    /**
     *
     * To get all uploaded folders in a course.
     * Gets uploaded folders using the file API.
     *
     * @return array $filestodownload Associative array with folder path as key and value returned from FILE API as its value
     *
     */
    public function get_material() {

        $course = $this->db->get_record('course', array('id' => $this->courseid), '*', MUST_EXIST);
        $modinfo = get_fast_modinfo($course);
        $coursematerial = array();
        $materialinfo = array();

        $filestodownload = array();
        $fs       = get_file_storage();
        foreach ($modinfo->instances as $modname => $instances) {
            if ($modname === $this->name) {
                foreach ($instances as $instancesid => $instance) {
                    if ($instance->uservisible) {
                        $coursematerial[$instance->id] = $instance;
                        $materialinfo[$instance->modname][] = $instance->id;
                    }
                }
            }
        }

        if ($course->format == "topics") {
            $subfolder = get_string('topic', 'block_course_download');
        } else {
            $subfolder = get_string('week', 'block_course_download');
        }

        foreach ($materialinfo as $materialname => $singlematerial) {
            $countofmaterial = count($singlematerial);
            for ($i = 0; $i < $countofmaterial; $i++) {
                $materialinfos = $coursematerial[$singlematerial[$i]];

                if (!$tmpfiles = $fs->get_file($materialinfos->context->id, 'mod_' . $materialname, 'content', '0', '/', '.')) {
                    $tmpfiles = null;
                }
                $sectid = $materialinfos->sectionnum;
                $directory = $subfolder . '_' . $sectid . '/' . $materialname . '/';
                $filestodownload[$directory . clean_filename($materialinfos->name)] = $tmpfiles;
            }
        }
        return $filestodownload;
    }
}