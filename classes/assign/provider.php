<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Course Download Block
 *
 * @package    block_course_download
 * @copyright  2019 onwards Yash Shah(yashah1234@gmail.com) and Kryselle Martis(krysellesimaranmartis@gmail.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * To get all relevant assignment submission and text in a particular context.
 *
 * @copyright  2019 onwards Yash Shah(yashah1234@gmail.com) and Kryselle Martis(krysellesimaranmartis@gmail.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class assign {
    /**
     *
     * Class constructor
     *
     * @param int $courseid The course id of the respective course.
     */
    public function __construct($courseid) {
        GLOBAL $DB;
        $this->name = get_string('assign', 'block_course_download');
        $this->courseid = $courseid;
        $this->db = $DB;
    }
    /**
     *
     * To get all assignment related files.
     * Gets uploaded files by using moodle File API and get assignment text
     * using Database queries.
     *
     * @return array $filestodownload Associative array with file name as key and either file_storage
     * or simple text from database
     *
     */
    public function get_material() {

        $course   = $this->db->get_record('course', array('id' => $this->courseid), '*', MUST_EXIST);
        $modinfo     = get_fast_modinfo($course);
        $coursematerial         = array();
        $materialinfo = array();

        $filestodownload = array();
        $fs       = get_file_storage();
        $modulenumberofassign = $this->db->get_records('modules', array('name' => 'assign'));
        $modulenumberofassign = (int) current($modulenumberofassign)->id;
        foreach ($modinfo->instances as $modname => $instances) {
            if ($modname === $this->name) {
                foreach ($instances as $instancesid => $instance) {
                    if ($instance->uservisible) {
                        $coursematerial[$instance->id] = $instance;
                        $materialinfo[$instance->modname][] = $instance->id;
                    }
                }
            }
        }

        if ($course->format == "topics") {
            $subfolder = get_string('topic', 'block_course_download');
        } else {
            $subfolder = get_string('week',  'block_course_download');
        }
        foreach ($materialinfo as $materialname => $singlematerial) {
            $countofmaterial = count($singlematerial);
            for ($i = 0; $i < $countofmaterial; $i++) {
                $materialinfos = $coursematerial[$singlematerial[$i]];
                $tmpfiles = $fs->get_area_files($materialinfos->context->id, 'assignsubmission_file', 'submission_files');
                reset($tmpfiles);
                $tmpfile  = current($tmpfiles);
                if ($tmpfile) {
                    $sectid = $materialinfos->sectionnum;
                    $directory = $subfolder.'_'.$sectid.'/'.$materialname.'/';
                    $tempfilename = clean_filename($materialinfos->name);
                    $tempextension = pathinfo(clean_filename($tmpfile->get_filename()), PATHINFO_EXTENSION);
                    if ($tempextension) {
                        $tempextension = '.'.$tempextension;
                    }
                    $filestodownload[$directory.$tempfilename.$tempextension] = $tmpfile;
                }
            }
        }
        $sectionrecords = $this->db->get_records('course_sections', array('course' => $this->courseid));
        foreach ($sectionrecords as $sectionrecord) {
            $sectid = $sectionrecord->section;
            $sequence = explode(',', $sectionrecord->sequence);
            for ($i = 0; $i < count($sequence); $i++) {
                $sequenceid = $sequence[$i];
                $moduleofsequence = $this->db->get_records('course_modules', array('id' => $sequenceid), '');
                $sequencemodule = (int) current($moduleofsequence)->module;
                if ($sequencemodule === $modulenumberofassign) {
                    $instance = (int) current($moduleofsequence)->instance;
                    $instancerecord = $this->db->get_records('assign', array('id' => $instance));
                    $instancerecord = current($instancerecord);

                    $nameofassign = $instancerecord->name;

                    $introofassign = $instancerecord->intro;
                    $introofassign = array($introofassign);

                    $directory = $subfolder.'_'.$sectid.'/assign/'.$nameofassign.'/';
                    $filestodownload[$directory.'assignment_text'] = $introofassign;
                }
            }
        }
        return $filestodownload;
    }
}