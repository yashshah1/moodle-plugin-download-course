<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Course Download Block
 *
 * @package    block_course_download
 * @copyright  2019 onwards Yash Shah(yashah1234@gmail.com) and Kryselle Martis(krysellesimaranmartis@gmail.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


require_once('../../config.php');
require_once($CFG->dirroot . '/lib/filelib.php');
require_once($CFG->dirroot . '/lib/moodlelib.php');

/*
 * For every entry in $allowed_download, there exists a class in
 * course_download/classes/
 *
 */

$alloweddownload = ["page", "folder", "resource", "assign"];


$courseid = required_param('courseid', PARAM_INT);
$course   = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
$zipper   = get_file_packer('application/zip');
$filename = str_replace(' ', '_', clean_filename($course->shortname."-".date("YmdHis"))); // Name of new zip file.


$filestodownload = array();
require_login($course);


$filestodownload[$filename.'/course_intro'] = array($course->summary);

for ($i = 0; $i < count($alloweddownload); $i++) {
    $item = $alloweddownload[$i];
    require_once("./classes/".$item."/provider.php");
    $material = new $item($courseid);
    $tempitems = $material->get_material();
    foreach ($tempitems as $name => $value) {
        $newname = $filename."/".$name;
        $filestodownload[$newname] = $value;
    }
}
// Zip files.
$tempzip = tempnam($CFG->tempdir.'/', get_string('materials', 'block_course_download').'_'.$course->shortname);

$zipper = new zip_packer();
$filename = $filename . ".zip";
if ($zipper->archive_to_pathname($filestodownload, $tempzip)) {
    send_temp_file($tempzip, $filename);
}