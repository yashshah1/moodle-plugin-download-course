<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Course Download Block
 *
 * @package    block_course_download
 * @copyright  2019 onwards Yash Shah(yashah1234@gmail.com) and Kryselle Martis(krysellesimaranmartis@gmail.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname']        = 'Download Course block';
$string['material_download'] = 'Download all materials';
$string['no_file_exist']  = 'No files available, add some to your course';
$string['download_files'] = 'Download all files';
$string['resource']       = 'resource';
$string['page']           = 'page';
$string['assign']         = 'assign';
$string['folder']         = 'folder';
$string['topic']          = 'Topic';
$string['week']           = 'Week';
$string['materials']      = 'Materials';
